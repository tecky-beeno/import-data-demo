import { knex } from './knex'

async function main() {
  await knex.migrate.rollback()
  await knex.migrate.latest()
  await knex.seed.run()
}
main().finally(() => {
  knex.destroy()
})
