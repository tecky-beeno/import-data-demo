import Knex from 'knex'

let configs = require('./knexfile')
let mode = process.env.NODE_ENV || 'development'
let config = configs[mode]
export let knex = Knex(config)
