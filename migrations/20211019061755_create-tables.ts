import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {
  if (!(await knex.schema.hasTable('subject'))) {
    await knex.schema.createTable('subject', table => {
      table.integer('id').unsigned().primary()
      table.text('name').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('grade'))) {
    await knex.schema.createTable('grade', table => {
      table.integer('id').unsigned().primary()
      table.text('name').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('chapter'))) {
    await knex.schema.createTable('chapter', table => {
      table.integer('id').unsigned().primary()
      table.integer('subject_id').notNullable().references('subject.id')
      table.integer('grade_id').notNullable().references('grade.id')
      table.text('name').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('question'))) {
    await knex.schema.createTable('question', table => {
      table.integer('id').unsigned().primary()
      table.integer('chapter_id').notNullable().references('chapter.id')
      table.integer('answer_id').nullable().references('question_option.id')
      table.text('explain').notNullable()
      table.text('question').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('question_option'))) {
    await knex.schema.createTable('question_option', table => {
      table.integer('id').unsigned().primary()
      table.integer('question_id').notNullable().references('question.id')
      table.text('option').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('question_image'))) {
    await knex.schema.createTable('question_image', table => {
      table.integer('id').unsigned().primary()
      table.integer('question_id').notNullable().references('question.id')
      table.timestamps(false, true)
    })
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('question_image')
  await knex.schema.dropTableIfExists('question_option')
  await knex.schema.dropTableIfExists('question')
  await knex.schema.dropTableIfExists('chapter')
  await knex.schema.dropTableIfExists('grade')
  await knex.schema.dropTableIfExists('subject')
}
