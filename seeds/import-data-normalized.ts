import { Knex } from 'knex'

interface SheetQuestion {
  question: string
  option1: string
  option2: string
  option3: string
  option4: string

  chapter: string
  subject: string
  grade: string
}

interface DBQuestion {
  question: string

  chapter_id: number
}

interface DBQuestionOption {
  question_id: number
  option: string
}

export async function seed(knex: Knex): Promise<void> {
  await knex('question').del()
  await knex('chapter').del()
  await knex('grade').del()
  await knex('subject').del()

  async function getSubjectId(name: string) {
    let row = await knex.select('id').from('subject').where({ name }).first()
    if (row) {
      return row.id
    }
    let rows = await knex.insert({ name }).into('subject').returning('id')
    return rows[0]
  }

  async function getGradeId(name: string) {
    let row = await knex.select('id').from('grade').where({ name }).first()
    if (row) {
      return row.id
    }
    let rows = await knex.insert({ name }).into('grade').returning('id')
    return rows[0]
  }

  async function getChapterId(combine: {
    chapter: string
    grade: string
    subject: string
  }) {
    let grade_id = await getGradeId(combine.grade)
    let subject_id = await getSubjectId(combine.subject)
    let row = await knex
      .select('id')
      .from('chapter')
      .where({
        name: combine.chapter,
        grade_id,
        subject_id,
      })
      .first()
    if (row) {
      return row.id
    }
    let rows = await knex
      .insert({
        name: combine.chapter,
        grade_id,
        subject_id,
      })
      .into('chapter')
      .returning('id')
    return rows[0]
  }

  let sheetRows: SheetQuestion[] = []

  for (let sheetRow of sheetRows) {
    let dbQuestion: DBQuestion = {
      question: sheetRow.question,
      chapter_id: await getChapterId(sheetRow),
    }
    const rows = await knex.insert(dbQuestion).into('question').returning('id')
    const question_id = rows[0] as number
    let options = [
      sheetRow.option1,
      sheetRow.option2,
      sheetRow.option3,
      sheetRow.option4,
    ]
    for (let option of options) {
      if (option) {
        let dbQuestionOption: DBQuestionOption = {
          question_id,
          option,
        }
        await knex.insert(dbQuestionOption).into('question_option')
      }
    }
  }
}
