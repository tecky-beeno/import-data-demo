import { Knex } from 'knex'

interface QuestionSheet {
  question: string
  option1: string
  option2: string
  option3: string
  option4: string

  chapter: string
  subject: string
  grade: string
}

interface QuestionDB {
  question: string
  option1: string
  option2: string
  option3: string
  option4: string

  chapter_id: number
}

export async function seed(knex: Knex): Promise<void> {
  await knex('question').del()
  await knex('chapter').del()
  await knex('grade').del()
  await knex('subject').del()

  async function getSubjectId(name: string) {
    let row = await knex.select('id').from('subject').where({ name }).first()
    if (row) {
      return row.id
    }
    let rows = await knex.insert({ name }).into('subject').returning('id')
    return rows[0]
  }

  async function getGradeId(name: string) {
    let row = await knex.select('id').from('grade').where({ name }).first()
    if (row) {
      return row.id
    }
    let rows = await knex.insert({ name }).into('grade').returning('id')
    return rows[0]
  }

  async function getChapterId(combine: {
    chapter: string
    grade: string
    subject: string
  }) {
    let grade_id = await getGradeId(combine.grade)
    let subject_id = await getSubjectId(combine.subject)
    let row = await knex
      .select('id')
      .from('chapter')
      .where({
        name: combine.chapter,
        grade_id,
        subject_id,
      })
      .first()
    if (row) {
      return row.id
    }
    let rows = await knex
      .insert({
        name: combine.chapter,
        grade_id,
        subject_id,
      })
      .into('chapter')
      .returning('id')
    return rows[0]
  }

  let sheetRows: QuestionSheet[] = []

  for (let sheetRow of sheetRows) {
    let dbRow: QuestionDB = {
      question: sheetRow.question,
      option1: sheetRow.option1,
      option2: sheetRow.option2,
      option3: sheetRow.option3,
      option4: sheetRow.option4,
      chapter_id: await getChapterId(sheetRow),
    }
    await knex.insert(dbRow).into('question')
  }
}
