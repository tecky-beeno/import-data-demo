import { Knex } from 'knex'

interface Chapter {
  subject: string
  grade: string
  name: string
  questions: Question[]
}

interface Question {
  question: string
  explain: string
  answer: string
  options: string[]
  images: string[]
}

let chapters: Chapter[] = [
  {
    subject: 'English',
    grade: 'Grade 1',
    name: 'Food',
    questions: [
      {
        question: 'What is banana?',
        explain: 'We can eat banana',
        options: ['food', 'toy', 'book'],
        answer: 'food',
        images: [],
      },
      {
        question: 'What is computer?',
        explain: 'We can eat banana',
        options: ['food', 'toy', 'book', 'electronics'],
        answer: 'electronics',
        images: [],
      },
    ],
  },
  {
    subject: 'Math',
    grade: 'Grade 3',
    name: 'multiply',
    questions: [
      {
        question: 'What is 1 x 1?',
        explain: '1 x anything = that thing',
        options: ['0', '1', '2', '3'],
        answer: '1',
        images: [],
      },
      {
        question: 'What is 2 x 3?',
        explain: '2 + 2 + 2 is 6',
        options: ['2', '3', '5', '6'],
        answer: '6',
        images: [],
      },
    ],
  },
]

export async function seed(knex: Knex): Promise<void> {
  await knex('question_image').del()
  await knex('question_option').del()
  await knex('question').del()
  await knex('chapter').del()
  await knex('grade').del()
  await knex('subject').del()

  async function getSubjectId(name: string) {
    let row = await knex.select('id').from('subject').where({ name }).first()
    if (row) {
      return row.id
    }
    let rows = await knex.insert({ name }).into('subject').returning('id')
    return rows[0]
  }

  async function getGradeId(name: string) {
    let row = await knex.select('id').from('grade').where({ name }).first()
    if (row) {
      return row.id
    }
    let rows = await knex.insert({ name }).into('grade').returning('id')
    return rows[0]
  }

  for (let chapter of chapters) {
    let grade_id = await getGradeId(chapter.grade)
    let subject_id = await getSubjectId(chapter.subject)
    let rows = await knex
      .insert({
        grade_id,
        subject_id,
        name: chapter.name,
      })
      .into('chapter')
      .returning('id')
    let chapter_id = rows[0]
    for (let question of chapter.questions) {
      rows = await knex
        .insert({
          chapter_id,
          question: question.question,
          explain: question.explain,
        })
        .into('question')
        .returning('id')
      let question_id = rows[0]
      let answer_id: any
      for (let option of question.options) {
        rows = await knex
          .insert({
            question_id,
            option: option,
          })
          .into('question_option')
          .returning('id')
        let option_id = rows[0]
        if (option == question.answer) {
          answer_id = option_id
        }
      }
      await knex('question').update({ answer_id }).where({ id: question_id })
    }
  }
}
